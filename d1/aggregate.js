// EXAMPLE OF AGGREGATE
db.fruits.aggregate([
        {$match: {onSale: true}  },
        {$group: {_id: "$supplier_id", total: {$sum:  "$stock" } } },
        {$sort: { total: 1} }

])



// To Match
db.fruits.aggregate([
        {$match: {onSale: true}  },

 ])       


// To Group
db.fruits.aggregate([
        {$group: {_id: "$supplier_id", total: {$sum:  "$stock" } } },
     
])

// To Sort
db.fruits.aggregate([
        {$sort: { total: 1} }

])

// To Deconstruct
db.fruits.aggregate([
        {$unwind:  "$origin" }

])


// To Count
db.scores.aggregate([
        { $match: {score: {$gte: 80} } },
        { $count: "passing_score" }
])


// To Group by Color 
db.fruits.aggregate([
        {$unwind:  "$color" },
        {$group: { _id: "$color", kinds: { $sum: 1 } } }
        
])

